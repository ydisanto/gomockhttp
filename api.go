package mockhttp

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
)

// TestingT is an interface wrapper around *testing.T
type TestingT interface {
	Error(args ...any)
	Errorf(format string, args ...any)
	Fatal(args ...any)
	Fatalf(format string, args ...any)
}

type ApiMock struct {
	testServer  *httptest.Server
	calls       map[HttpCall]http.HandlerFunc
	testState   TestingT
	invocations map[HttpCall][]*Invocation
}

type HttpCall struct {
	Method string
	Path   string
}

func Api(testState TestingT) *ApiMock {
	mockedApi := &ApiMock{
		calls:       map[HttpCall]http.HandlerFunc{},
		testState:   testState,
		invocations: map[HttpCall][]*Invocation{},
	}

	testServer := httptest.NewServer(http.HandlerFunc(func(res http.ResponseWriter, request *http.Request) {

		call := HttpCall{
			Method: strings.ToLower(request.Method),
			Path:   request.RequestURI,
		}

		invocations := mockedApi.invocations[call]
		invocations = append(invocations, newInvocation(request, testState))
		mockedApi.invocations[call] = invocations

		handler := mockedApi.calls[call]
		if handler != nil {
			handler(res, request)
		} else {
			res.WriteHeader(404)
			testState.Fatalf("unmocked invocation %s %s\n", call.Method, call.Path)
		}
	}))
	mockedApi.testServer = testServer

	return mockedApi
}

func (mockedApi *ApiMock) Close() {
	mockedApi.testServer.Close()
}

func (mockedApi *ApiMock) GetUrl() *url.URL {
	testServerUrl, err := url.Parse(mockedApi.testServer.URL)
	if err != nil {
		mockedApi.testState.Fatal(err)
	}
	return testServerUrl
}

func (mockedApi *ApiMock) GetHost() string {
	return mockedApi.GetUrl().Host
}

func (mockedApi *ApiMock) Stub(method string, path string) *StubBuilder {
	return &StubBuilder{
		api: mockedApi,
		call: &HttpCall{
			Method: strings.ToLower(method),
			Path:   path,
		},
	}
}

func (mockedApi *ApiMock) Verify(method string, path string) *CallVerifier {
	return &CallVerifier{
		api: mockedApi,
		call: &HttpCall{
			Method: strings.ToLower(method),
			Path:   path,
		},
	}
}
